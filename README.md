## Installation

```bash
cd react-code-sample
yarn install
```

## Get started

```bash
yarn start
```
